FROM node:14-alpine as build
RUN apk update && apk add p7zip curl jq graphviz
WORKDIR /src
COPY src .
RUN npm install
ENTRYPOINT [ "/bin/sh" ]
